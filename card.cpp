//(c) Ryan Weston
//raw124@zips.uakron.edu

#include "card.hpp"
#include <vector>
#include <iostream>

void make_standard_deck(Deck &d)
{
    Suited c1{0, Ace, Spades};
    d.cards.push_back(&c1);
    Suited c2{1, Two, Spades};
    d.cards.push_back(&c2);
    Suited c3{2, Three, Spades};
    d.cards.push_back(&c3);
    Suited c4{3, Four, Spades};
    d.cards.push_back(&c4);
    Suited c5{4, Five, Spades};
    d.cards.push_back(&c5);
    Suited c6{5, Six, Spades};
    d.cards.push_back(&c6);
    Suited c7{6, Seven, Spades};
    d.cards.push_back(&c7);
    Suited c8{7, Eight, Spades};
    d.cards.push_back(&c8);
    Suited c9{8, Nine, Spades};
    d.cards.push_back(&c9);
    Suited c10{9, Ten, Spades};
    d.cards.push_back(&c10);
    Suited c11{10, Jack, Spades};
    d.cards.push_back(&c11);
    Suited c12{11, Queen, Spades};
    d.cards.push_back(&c12);
    Suited c13{12, King, Spades};
    d.cards.push_back(&c13);
    Suited c14{13, Ace, Clubs};
    d.cards.push_back(&c14);
    Suited c15{14, Two, Clubs};
    d.cards.push_back(&c15);
    Suited c16{15, Three, Clubs};
    d.cards.push_back(&c16);
    Suited c17{16, Four, Clubs};
    d.cards.push_back(&c17);
    Suited c18{17, Five, Clubs};
    d.cards.push_back(&c18);
    Suited c19{18, Six, Clubs};
    d.cards.push_back(&c19);
    Suited c20{19, Seven, Clubs};
    d.cards.push_back(&c20);
    Suited c21{20, Eight, Clubs};
    d.cards.push_back(&c21);
    Suited c22{21, Nine, Clubs};
    d.cards.push_back(&c22);
    Suited c23{22, Ten, Clubs};
    d.cards.push_back(&c23);
    Suited c24{23, Jack, Clubs};
    d.cards.push_back(&c24);
    Suited c25{24, Queen, Clubs};
    d.cards.push_back(&c25);
    Suited c26{25, King, Clubs};
    d.cards.push_back(&c26);
    Suited c27{26, Ace, Hearts};
    d.cards.push_back(&c27);
    Suited c28{27, Two, Hearts};
    d.cards.push_back(&c28);
    Suited c29{28, Three, Hearts};
    d.cards.push_back(&c29);
    Suited c30{29, Four, Hearts};
    d.cards.push_back(&c30);
    Suited c31{30, Five, Hearts};
    d.cards.push_back(&c31);
    Suited c32{31, Six, Hearts};
    d.cards.push_back(&c32);
    Suited c33{32, Seven, Hearts};
    d.cards.push_back(&c33);
    Suited c34{33, Eight, Hearts};
    d.cards.push_back(&c34);
    Suited c35{34, Nine, Hearts};
    d.cards.push_back(&c35);
    Suited c36{35, Ten, Hearts};
    d.cards.push_back(&c36);
    Suited c37{36, Jack, Hearts};
    d.cards.push_back(&c37);
    Suited c38{37, Queen, Hearts};
    d.cards.push_back(&c38);
    Suited c39{38, King, Hearts};
    d.cards.push_back(&c39);
    Suited c40{39, Ace, Diamonds};
    d.cards.push_back(&c40);
    Suited c41{40, Two, Diamonds};
    d.cards.push_back(&c41);
    Suited c42{41, Three, Diamonds};
    d.cards.push_back(&c42);
    Suited c43{42, Four, Diamonds};
    d.cards.push_back(&c43);
    Suited c44{43, Five, Diamonds};
    d.cards.push_back(&c44);
    Suited c45{44, Six, Diamonds};
    d.cards.push_back(&c45);
    Suited c46{45, Seven, Diamonds};
    d.cards.push_back(&c46);
    Suited c47{46, Eight, Diamonds};
    d.cards.push_back(&c47);
    Suited c48{47, Nine, Diamonds};
    d.cards.push_back(&c48);
    Suited c49{48, Ten, Diamonds};
    d.cards.push_back(&c49);
    Suited c50{49, Jack, Diamonds};
    d.cards.push_back(&c50);
    Suited c51{50, Queen, Diamonds};
    d.cards.push_back(&c51);
    Suited c52{51, King, Diamonds};
    d.cards.push_back(&c52);
    Joker j1{52, Black};
    Joker j2{53, Red};
    d.cards.push_back(&j1);
    d.cards.push_back(&j2);
}
void print(const Deck &deck)
{
    for(int i = 0; i <= 51; i++)
    {
        Card* p = deck.cards[i];
        Suited* c1 = static_cast<Suited*>(p);
        std::cout << c1->rank << " " << c1->suit << std::endl;
    }
}
