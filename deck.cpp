//(c) Ryan Weston
//raw124@zips.uakron.edu

#include "deck.hpp"
#include "player.hpp"
#include "pile.hpp"
#include <iostream>
#include <random>
#include <algorithm>

Deck make_standard_deck()
{
    Deck d {
        {Ace, Spades},
        {Two, Spades},
        {Three, Spades},
        {Four, Spades},
        {Five, Spades},
        {Six, Spades},
        {Seven, Spades},
        {Eight, Spades},
        {Nine, Spades},
        {Ten, Spades},
        {Jack, Spades},
        {Queen, Spades},
        {King, Spades},

        {Ace, Clubs},
        {Two, Clubs},
        {Three, Clubs},
        {Four, Clubs},
        {Five, Clubs},
        {Six, Clubs},
        {Seven, Clubs},
        {Eight, Clubs},
        {Nine, Clubs},
        {Ten, Clubs},
        {Jack, Clubs},
        {Queen, Clubs},
        {King, Clubs},

        {Ace, Hearts},
        {Two, Hearts},
        {Three, Hearts},
        {Four, Hearts},
        {Five, Hearts},
        {Six, Hearts},
        {Seven, Hearts},
        {Eight, Hearts},
        {Nine, Hearts},
        {Ten, Hearts},
        {Jack, Hearts},
        {Queen, Hearts},
        {King, Hearts},

        {Ace, Diamonds},
        {Two, Diamonds},
        {Three, Diamonds},
        {Four, Diamonds},
        {Five, Diamonds},
        {Six, Diamonds},
        {Seven, Diamonds},
        {Eight, Diamonds},
        {Nine, Diamonds},
        {Ten, Diamonds},
        {Jack, Diamonds},
        {Queen, Diamonds},
        {King, Diamonds},
    };
    return d;
}
Deck make_deck(const Deck& d1, const Deck& d2)
{
    Deck d;
    d.insert(d.end(), d1.begin(), d1.end());
    d.insert(d.end(), d2.begin(), d2.end());
    return d;
}
void shuffle(Deck& deck)
{
    extern std::minstd_rand prng;
    extern std::random_device rng;
    prng.seed(rng());
    std::shuffle(deck.begin(), deck.end(), prng);
}

void print(const Deck &deck)
{
    int i = 1;
    for (SuitedCard c : deck) {
        std::cout << c << ' ';
        if (i % 13 == 0) {
        std::cout << '\n';
        i = 0;
        }
        ++i;
    }
    std::cout << '\n';
}
void deal(Deck &deck, Player &p1, Player &p2)
{
    shuffle(deck);
    int i = 1;
    for (SuitedCard c : deck)
    {
        if(i % 2 == 0)
        {
            p2.hand.push(c);
            ++p2.cardCount;
        }
        else
        {
            p1.hand.push(c);
            ++p1.cardCount;
        }
        ++i;
    }
}
void play(Deck &deck, Player &p1, Player &p2, Pile &pile)
{
    if (p1.cardCount <= 0 || p2.cardCount <= 0)
        return;
    SuitedCard c1 = pop(p1.hand);
    SuitedCard c2 = pop(p2.hand);
    --p1.cardCount;
    --p2.cardCount;
    std::cout << c1 << " vs " << c2 << std::endl;
    pile.pile.push(c1);
    ++pile.cardCount;
    pile.pile.push(c2);
    ++pile.cardCount;
    if (c2.getRank() < c1.getRank())
    {
        std::cout << "P1 wins" << std::endl;
        while(pile.cardCount != 0)
        {
            SuitedCard c = pop(pile.pile);
            --pile.cardCount;
            p1.hand.push(c);
            ++p1.cardCount;
        }
    }
    else if (c1.getRank() < c2.getRank())
    {
        std::cout << "P2 wins" << std::endl;
        while(pile.cardCount != 0)
        {
            SuitedCard c = pop(pile.pile);
            --pile.cardCount;
            p2.hand.push(c);
            ++p2.cardCount;
        }
    }
    else if (c2.getRank() == c1.getRank())
    {
        std::cout << "WAR" << std::endl;
        if (p1.cardCount <= 0)
        {
            while(pile.cardCount != 0)
            {
                SuitedCard c = pop(pile.pile);
                --pile.cardCount;
                p2.hand.push(c);
                ++p2.cardCount;
            }
            return;
        }
        else if (p2.cardCount <= 0)
        {
            while(pile.cardCount != 0)
            {
                SuitedCard c = pop(pile.pile);
                --pile.cardCount;
                p1.hand.push(c);
                ++p1.cardCount;
            }
            return;
        }
        if (p1.cardCount >= 2 || p2.cardCount >= 2)
        {
            SuitedCard c3 = pop(p1.hand);
            SuitedCard c4 = pop(p2.hand);
            --p1.cardCount;
            --p2.cardCount;
            pile.pile.push(c3);
            ++pile.cardCount;
            pile.pile.push(c4);
            ++pile.cardCount;
        }
        play(deck, p1, p2, pile);
    }
}




