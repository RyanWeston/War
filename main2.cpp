//(c) Ryan Weston
//raw124@zips.uakron.edu
#include <iostream>
#include "calc2.hpp"

using namespace std;

int main()
{
    //(3 - 1) * (4 + 2)
    Expr* e = new Mul(
        new Sub(
            new Int(3),
            new Int(1)
        ),
        new Add(
            new Int(4),
            new Int(2)
        )
    );
    std::cout << e << std::endl;
    while (!e->isValue()) {
        e = e->reduce();
        std::cout << e << '\n';
    }
    delete e;
    return 0;
}
