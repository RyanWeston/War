//(c) Ryan Weston
//raw124@zips.uakron.edu
#ifndef PLAYER_HPP
#define PLAYER_HPP

#include "queue.hpp"

struct Player
{
    CardQueue hand;
    int cardCount;
public:
    Player() {cardCount = 0;}
    bool empty();
};

#endif //PLAYER_HPP
