//(c) Ryan Weston
//raw124@zips.uakron.edu
#include "calc2.hpp"

bool operator==(const Expr& e1, const Expr& e2)
{
  return e1.equal(&e2);
}
std::ostream& operator<<(std::ostream& os, const Expr* e)
{
  e->print(os);
  return os;
}
