//(c) Ryan Weston
//raw124@zips.uakron.edu
#ifndef PILE_HPP
#define PILE_HPP

#include "card.hpp"
#include <queue>

struct Pile
{
    CardQueue pile;
    int cardCount;
public:
    Pile() {cardCount = 0;}
    bool empty();
};

#endif //PILE_HPP
