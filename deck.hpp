//(c) Ryan Weston
//raw124@zips.uakron.edu
#ifndef DECK_HPP
#define DECK_HPP

#include "card.hpp"
#include "player.hpp"
#include "pile.hpp"
#include <vector>

using Deck = std::vector<SuitedCard>;

Deck make_standard_deck();

Deck make_deck(const Deck& d1, const Deck& d2);

void shuffle(Deck& deck);

void print(const Deck &deck);

void deal(Deck& d1, Player& p1, Player& p2);

void play(Deck& d1, Player& p1, Player& p2, Pile &pile);

#endif //DECK_HPP
