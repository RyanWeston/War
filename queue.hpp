//(c) Ryan Weston
//raw124@zips.uakron.edu
#ifndef QUEUE_HPP
#define QUEUE_HPP

#include "card.hpp"
#include <queue>

using CardQueue = std::queue<SuitedCard>;

template<class Q>
typename Q::value_type pop(Q& q) {
    auto result = q.front();
    q.pop();
    return result;
};

#endif //QUEUE_HPP
