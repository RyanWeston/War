//(c) Ryan Weston
//raw124@zips.uakron.edu
#ifndef CALC_HPP
#define CALC_HPP

#include <iostream>
#include <stdexcept>

struct Expr;
struct Int;
struct Add;
struct Sub;
struct Mul;
struct Div;

struct Expr
{
    virtual ~Expr() = default;
    virtual Expr* clone() const = 0;
    virtual void print(std::ostream& os) const = 0;
    virtual int evaluate() const = 0;
    virtual Expr* reduce() const = 0;
    virtual bool isValue() const { return false;}
    virtual bool equal(const Expr* that) const = 0;
    virtual bool equal(const Int* that) const { return false; }
    virtual bool equal(const Add* that) const { return false; }
    virtual bool equal(const Sub* that) const { return false; }
    virtual bool equal(const Mul* that) const { return false; }
    virtual bool equal(const Div* that) const { return false; }
};
bool operator==(const Expr& e1, const Expr& e2);
std::ostream& operator<<(std::ostream& os, const Expr* e);
struct Int : Expr
{
    Int(int n) : val(n) { }
    Int(const Int& n) : val(n.val) { }
    Int* clone() const override {
        return new Int(*this);
    }
    void print(std::ostream& os) const override {
        os << val;
    }
    int evaluate() const override {
        return val;
    }
    Expr* reduce() const override {
        throw std::runtime_error("already reduced");
    }
    bool isValue() const override { return true; }
    bool equal(const Expr* that) const override {
        return that->equal(this);
    }
    bool equal(const Int* that) const override {
        return val == that->val;
    }
    int val;
};
struct Binary : Expr
{
    Binary(Expr* e1, Expr* e2) : e1(e1), e2(e2) { }
    Binary(const Binary& e) : e1(e.e1->clone()), e2(e.e2->clone()) { }
    ~Binary() override {
        delete e1;
        delete e2;
    }
     static void print_enclosed(std::ostream& os, const Expr *e)
    {
        os << '(';
        e->print(os);
        os << ')';
    }
    Expr* e1;
    Expr* e2;
};
struct Add : Binary
{
    using Binary::Binary;
    Add* clone() const override {
        return new Add(*this);
    }
    void print(std::ostream& os) const override {
        print_enclosed(os, e1);
        os << " + ";
        print_enclosed(os, e2);
    }
    int evaluate() const override {
        return e1->evaluate() + e2->evaluate();
    }
    Expr* reduce() const override {
        if (e1->isValue()) {
        if (e2->isValue())
            return new Int(evaluate());
        return new Add(e1, e2->reduce());
        }
        return new Add(e1->reduce(), e2);
    }
    bool equal(const Expr *that) const override {
        return that->equal(this);
    }
    bool equal(const Add* that) const override {
        return (e1 == that->e1) && (e2 == that->e2);
    }
};
struct Sub : Binary
{
    using Binary::Binary;
    Sub* clone() const override {
        return new Sub(*this);
    }
    void print(std::ostream& os) const override {
        print_enclosed(os, e1);
        os << " - ";
        print_enclosed(os, e2);
    }
    int evaluate() const override {
        return e1->evaluate() - e2->evaluate();
    }
    Expr* reduce() const override {
        if (e1->isValue()) {
        if (e2->isValue())
            return new Int(evaluate());
        return new Sub(e1, e2->reduce());
        }
        return new Sub(e1->reduce(), e2);
    }
    bool equal(const Expr *that) const override {
        return that->equal(this);
    }
    bool equal(const Sub* that) const override {
        return (e1 == that->e1) && (e2 == that->e2);
    }
};
struct Mul : Binary
{
    using Binary::Binary;
    Mul* clone() const override {
        return new Mul(*this);
    }
    void print(std::ostream& os) const override {
        print_enclosed(os, e1);
        os << " * ";
        print_enclosed(os, e2);
    }
    int evaluate() const override {
        return e1->evaluate() * e2->evaluate();
    }
    Expr* reduce() const override {
        if (e1->isValue()) {
        if (e2->isValue())
            return new Int(evaluate());
        return new Mul(e1, e2->reduce());
        }
        return new Mul(e1->reduce(), e2);
    }
    bool equal(const Expr *that) const override {
        return that->equal(this);
    }
    bool equal(const Mul* that) const override {
        return (e1 == that->e1) && (e2 == that->e2);
    }
};
struct Div : Binary
{
    using Binary::Binary;
    Div* clone() const override {
        return new Div(*this);
    }
    void print(std::ostream& os) const override {
        print_enclosed(os, e1);
        os << " / ";
        print_enclosed(os, e2);
    }
    int evaluate() const override {
        return e1->evaluate() / e2->evaluate();
    }
    Expr* reduce() const override {
        if (e1->isValue()) {
        if (e2->isValue())
            return new Int(evaluate());
        return new Div(e1, e2->reduce());
        }
        return new Div(e1->reduce(), e2);
    }
    bool equal(const Expr *that) const override {
        return that->equal(this);
    }
    bool equal(const Div* that) const override {
        return (e1 == that->e1) && (e2 == that->e2);
    }
};
#endif // CALC_HPP
