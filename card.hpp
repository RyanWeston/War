//(c) Ryan Weston
//raw124@zips.uakron.edu
#ifndef CARD_HPP
#define CARD_HPP

#include <iostream>
#include <vector>

enum Rank
{
    Ace,
    Two,
    Three,
    Four,
    Five,
    Six,
    Seven,
    Eight,
    Nine,
    Ten,
    Jack,
    Queen,
    King,
};
enum Suit
{
    Spades,
    Clubs,
    Hearts,
    Diamonds,
};
enum Color
{
    Black,
    Red,
};
struct Card
{
    Card(int id) : id(id) {}
    int id;
};
struct Suited : Card
{
    Suited(int id, Rank r, Suit s) :  Card(id), rank(r), suit(s) {}
    Rank rank;
    Suit suit;
};
struct Joker : Card
{
    Joker(int id, Color c) : Card(id), color(c) {}
    Color color;
};
struct Deck
{
    std::vector<Card*> cards;
};
void make_standard_deck(Deck &d);
void print(const Deck &deck);
#endif // CARD_HPP
