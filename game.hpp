//(c) Ryan Weston
//raw124@zips.uakron.edu
#ifndef GAME_HPP
#define GAME_HPP

#include "player.hpp"

struct Options
{
    bool use_jokers = false;
    int num_decks = 1;
    bool ace_high = true;
    int num_sacrifice = 1;
    bool negotiable_sacrifice = true;
};

struct Game
{
    Game(Options opts);
    Game() {Options opts;}

    void step();
    void run();

    Options opts;
    Deck deck;
    Player p1;
    Player p2;
    Pile pile;
};

#endif //GAME_HPP
